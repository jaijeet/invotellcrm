package com.invotell.crm.user.service;

import java.util.List;

import com.invotell.crm.comm.exception.InvoTellServiceException;
import com.invotell.crm.comm.exception.InvoTellValidationException;
import com.invotell.crm.comm.exception.NotFoundException;
import com.invotell.crm.user.dao.entity.Category;

public interface CategoryService
{
	Category addNewCategory(Category category) throws InvoTellServiceException, InvoTellValidationException;

	List<Category> getAllCategory(boolean fetchSkill) throws InvoTellServiceException, NotFoundException;

	void deleteCategory(String name) throws InvoTellServiceException, InvoTellValidationException, NotFoundException;

	Category getCategoryByName(String name)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException;
}
