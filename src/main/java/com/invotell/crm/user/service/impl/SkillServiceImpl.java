package com.invotell.crm.user.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invotell.crm.comm.exception.InvoTellServiceException;
import com.invotell.crm.comm.exception.InvoTellValidationException;
import com.invotell.crm.comm.exception.NotFoundException;
import com.invotell.crm.comm.model.InvoTellError;
import com.invotell.crm.user.dao.entity.Category;
import com.invotell.crm.user.dao.entity.Skill;
import com.invotell.crm.user.dao.repository.CategoryRepository;
import com.invotell.crm.user.dao.repository.SkillRepository;
import com.invotell.crm.user.service.SkillService;

@Service
public class SkillServiceImpl implements SkillService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SkillServiceImpl.class);

	@Autowired
	private SkillRepository skillRepository;

	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public Skill addNewSkill(Skill skillToSave, String categoryName)
			throws InvoTellServiceException, InvoTellValidationException
	{
		Skill result = null;
		try
		{

			if (skillToSave == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Skill details are required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			Category existedCategory = categoryRepository.findCategoryByName(categoryName,false);
			if (existedCategory == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST,
						"Category named [" + categoryName + "] not found");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			Skill existedSkill = skillRepository.getSkillByName(skillToSave.getName(), existedCategory.getName());

			if (existedSkill != null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST,
						"Skill with name [" + skillToSave.getName() + "] already exists");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			skillToSave.setSkillCategory(existedCategory.getName());

			result = skillRepository.addNewSkill(skillToSave);
		} catch (InvoTellValidationException itve)
		{
			throw itve;
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}

	@Override
	public List<Skill> getAllSkill() throws InvoTellServiceException, NotFoundException
	{
		List<Skill> skillList = null;
		try
		{
			skillList = skillRepository.getAllSkillList();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return skillList;
	}

	@Override
	public void deleteSkill(String skillName, String categoryName)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException
	{
		try
		{
			if (skillName == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Skill name is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			Category existedCategory = categoryRepository.findCategoryByName(categoryName,false);
			if (existedCategory == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST,
						"Category named [" + categoryName + "] not found");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			Skill existingSkill = skillRepository.getSkillByName(skillName, existedCategory.getName());

			if (existingSkill == null)
			{
				NotFoundException notFoundExcp = new NotFoundException("Skill with name [" + skillName + "] not found");
				throw notFoundExcp;
			}

			skillRepository.deleteSkill(existingSkill);
		} catch (InvoTellValidationException validationException)
		{
			throw validationException;
		} catch (NotFoundException notFoundExcp)
		{
			throw notFoundExcp;
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}

	}

	@Override
	public Skill getSkillByName(String skillName, String categoryName)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException
	{
		Skill result = null;
		try
		{
			if (skillName == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Skill name is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			Category existedCategory = categoryRepository.findCategoryByName(categoryName,false);
			if (existedCategory == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST,
						"Category named [" + categoryName + "] not found");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			Skill existingSkill = skillRepository.getSkillByName(skillName, existedCategory.getName());
			if(existingSkill==null)
			{
				NotFoundException validationException = new NotFoundException(
						"Skill with name [" + skillName + "] doesn't found");
				throw validationException;
			}
			result = existingSkill;
		} catch (InvoTellValidationException validationException)
		{
			throw validationException;
		} catch (NotFoundException notFoundExcp)
		{
			throw notFoundExcp;
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
		return result;
	}

	@Override
	public List<Skill> getAllSkillByCategory(String categoryName)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException
	{
		List<Skill> skillList = null;
		try
		{
			if (categoryName == null || categoryName.trim().equals(""))
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Category name is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			Category existedCategory = categoryRepository.findCategoryByName(categoryName,false);
			if (existedCategory == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST,
						"Category named [" + categoryName + "] not found");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			skillList = skillRepository.getSkillListByCategoryName(categoryName);
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return skillList;
	}
	
}
