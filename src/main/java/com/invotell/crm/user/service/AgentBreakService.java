package com.invotell.crm.user.service;

import java.util.List;

import com.invotell.crm.comm.exception.InvoTellServiceException;
import com.invotell.crm.comm.exception.InvoTellValidationException;
import com.invotell.crm.comm.exception.NotFoundException;
import com.invotell.crm.user.dao.entity.AgentBreak;

public interface AgentBreakService
{
	AgentBreak addNewAgentBreak(AgentBreak agentBreak) throws InvoTellServiceException, InvoTellValidationException;

	List<AgentBreak> getAllAgentBreak() throws InvoTellServiceException, NotFoundException;

	void deleteAgentBreak(String breakName) throws InvoTellServiceException, InvoTellValidationException, NotFoundException;

	AgentBreak getAgentBreakByName(String breakName)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException;
}
