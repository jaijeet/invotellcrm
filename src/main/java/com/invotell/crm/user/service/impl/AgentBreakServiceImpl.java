package com.invotell.crm.user.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invotell.crm.comm.exception.InvoTellPersistanceException;
import com.invotell.crm.comm.exception.InvoTellServiceException;
import com.invotell.crm.comm.exception.InvoTellValidationException;
import com.invotell.crm.comm.exception.NotFoundException;
import com.invotell.crm.comm.model.InvoTellError;
import com.invotell.crm.user.dao.entity.AgentBreak;
import com.invotell.crm.user.dao.entity.Category;
import com.invotell.crm.user.dao.repository.AgentBreakRepository;
import com.invotell.crm.user.service.AgentBreakService;

@Service
public class AgentBreakServiceImpl implements AgentBreakService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AgentBreakServiceImpl.class);
	
	@Autowired
	private AgentBreakRepository agentBreakRepository;

	
	@Override
	public AgentBreak addNewAgentBreak(AgentBreak agentBreak)
			throws InvoTellServiceException, InvoTellValidationException
	{
		AgentBreak result = null;
		try
		{

			if (agentBreak == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Agent break is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			AgentBreak existedBreak = agentBreakRepository.findBreakByName(agentBreak.getBreakName());
			if (existedBreak != null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST,
						"Agent break with name [" + agentBreak.getBreakName() + "] already exists");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			result = agentBreakRepository.saveAgentBreak(agentBreak);
		} catch (InvoTellValidationException itve)
		{
			throw itve;
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}

	@Override
	public List<AgentBreak> getAllAgentBreak() throws InvoTellServiceException, NotFoundException
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteAgentBreak(String name) throws InvoTellServiceException, InvoTellValidationException, NotFoundException
	{
		try
		{
			if (name == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Category name is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			AgentBreak existingAgentBreak = agentBreakRepository.findBreakByName(name);

			if (existingAgentBreak==null || !name.equals(existingAgentBreak.getBreakName()))
			{
				NotFoundException notFoundExcp = new NotFoundException("Agent Break with name [" + name + "] not found");
				throw notFoundExcp;
			}

			agentBreakRepository.deleteAgentBreak(existingAgentBreak);

		} catch (InvoTellValidationException validationException)
		{
			throw validationException;
		} catch (NotFoundException notFoundExcp)
		{
			throw notFoundExcp;
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
	}

	@Override
	public AgentBreak getAgentBreakByName(String name)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException
	{
		AgentBreak result = null;
		try
		{
			if (name == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "AgentBreak name is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			result = agentBreakRepository.findBreakByName(name);
			if (result == null)
			{
				NotFoundException validationException = new NotFoundException(
						"Agent Break with name [" + name + "] doesn't found");
				throw validationException;
			}
		} catch (NotFoundException nfe)
		{
			LOGGER.error(nfe.getMessage(), nfe);
			throw nfe;
		} catch (InvoTellPersistanceException ive)
		{
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}

		return result;
	}

}
