package com.invotell.crm.user.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invotell.crm.comm.exception.InvoTellPersistanceException;
import com.invotell.crm.comm.exception.InvoTellServiceException;
import com.invotell.crm.comm.exception.InvoTellValidationException;
import com.invotell.crm.comm.exception.NotFoundException;
import com.invotell.crm.comm.model.InvoTellError;
import com.invotell.crm.user.dao.entity.Category;
import com.invotell.crm.user.dao.repository.CategoryRepository;
import com.invotell.crm.user.service.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService
{

	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryServiceImpl.class);

	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public Category addNewCategory(Category category) throws InvoTellServiceException, InvoTellValidationException
	{
		Category result = null;
		try
		{

			if (category == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Category is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			Category existedCategory = categoryRepository.findCategoryByName(category.getName(),false);
			if (existedCategory != null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST,
						"Category with name [" + category.getName() + "] already exists");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			result = categoryRepository.saveCategory(category);
		} catch (InvoTellValidationException itve)
		{
			throw itve;
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}

	@Override
	public List<Category> getAllCategory(boolean fetchSkill) throws InvoTellServiceException, NotFoundException
	{
		List<Category> categoryList = null;
		try
		{
			categoryList = categoryRepository.findAllCategories(fetchSkill);
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return categoryList;
	}

	@Override
	public void deleteCategory(String name)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException
	{
		try
		{
			if (name == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Category name is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			Category existingCategory = categoryRepository.findCategoryByName(name,false);

			if (!name.equals(existingCategory.getName()))
			{
				NotFoundException notFoundExcp = new NotFoundException("Category with name [" + name + "] not found");
				throw notFoundExcp;
			}

			categoryRepository.deleteCategory(existingCategory);

		} catch (InvoTellValidationException validationException)
		{
			throw validationException;
		} catch (NotFoundException notFoundExcp)
		{
			throw notFoundExcp;
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}

	}

	@Override
	public Category getCategoryByName(String name)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException
	{
		Category result = null;
		try
		{
			if (name == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Category name is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			result = categoryRepository.findCategoryByName(name,true);
			if (result == null)
			{
				NotFoundException validationException = new NotFoundException(
						"Category with name [" + name + "] doesn't found");
				throw validationException;
			}
		} catch (NotFoundException nfe)
		{
			LOGGER.error(nfe.getMessage(), nfe);
			throw nfe;
		} catch (InvoTellPersistanceException ive)
		{
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}

		return result;
	}

}
