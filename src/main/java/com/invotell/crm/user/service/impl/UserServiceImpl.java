package com.invotell.crm.user.service.impl;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.invotell.crm.comm.exception.InvoTellPersistanceException;
import com.invotell.crm.comm.exception.InvoTellServiceException;
import com.invotell.crm.comm.exception.InvoTellValidationException;
import com.invotell.crm.comm.exception.NotFoundException;
import com.invotell.crm.comm.model.InvoTellError;
import com.invotell.crm.user.dao.entity.Skill;
import com.invotell.crm.user.dao.entity.User;
import com.invotell.crm.user.dao.repository.SkillRepository;
import com.invotell.crm.user.dao.repository.UserRepository;
import com.invotell.crm.user.service.SkillService;
import com.invotell.crm.user.service.UserService;

@Service
public class UserServiceImpl implements UserService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private SkillService skillService;

	@Override
	public User addNewUser(User user) throws InvoTellServiceException, InvoTellValidationException
	{
		User result = null;
		try
		{

			if (user == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "User details are required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			User existedUser = userRepository.findUserByEmail(user.getEmail());
			if (existedUser != null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST,
						"User with email [" + user.getEmail() + "] already exists");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			
			
			List<Skill> userSkills  = user.getUserSkills();
			verifySkillExist(userSkills);
			result = userRepository.saveUser(user);
		} catch (InvoTellValidationException itve)
		{
			throw itve;
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return result;
	}

	@Override
	public User getUserByEmail(String email)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException
	{
		User result = null;
		try
		{
			if (email == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "User email is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}
			result = userRepository.findUserByEmail(email);
			if (result == null)
			{
				NotFoundException validationException = new NotFoundException(
						"User with email [" + email + "] doesn't found");
				throw validationException;
			}
		} catch (NotFoundException nfe)
		{
			LOGGER.error(nfe.getMessage(), nfe);
			throw nfe;
		} catch (InvoTellPersistanceException ive)
		{
			LOGGER.error(ive.getMessage(), ive);
			throw new InvoTellServiceException(ive.getMessage());
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}

		return result;
	}

	@Override
	public User updateUser(String email, User userToUpdate) throws InvoTellServiceException
	{
		User result = null;
		try
		{
			if (userToUpdate == null)
			{
				// validation exception should be there
			}

			User existingUser = userRepository.findUserByEmail(email);

			if (!userToUpdate.getEmail().equals(existingUser.getEmail()))
			{
				User validate = userRepository.findUserByEmail(userToUpdate.getEmail());
				if (validate != null)
				{
					// validation exception should be there
				}
			}
			updateEntity(userToUpdate, existingUser);

			userRepository.saveUser(existingUser);

		} catch (Exception e)
		{
			// TODO: handle exception
		}
		return result;
	}

	@Override
	public List<User> getAllUsers(boolean fetchSkills) throws InvoTellServiceException, NotFoundException
	{
		List<User> userList = null;
		try
		{
			userList = userRepository.findAllUser(fetchSkills);
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e.getMessage());
		}
		return userList;
	}

	@Override
	public void deleteUser(String email) throws InvoTellServiceException, InvoTellValidationException, NotFoundException
	{
		try
		{
			if (email == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Email is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			User existingUser = userRepository.findUserByEmail(email);

			if (!email.equals(existingUser.getEmail()))
			{
				NotFoundException notFoundExcp = new NotFoundException("User with email [" + email + "] not found");
				throw notFoundExcp;
			}

			userRepository.deleteUser(existingUser);

		} catch (InvoTellValidationException validationException)
		{
			throw validationException;
		} catch (NotFoundException notFoundExcp)
		{
			throw notFoundExcp;
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
	}

	@Override
	public User authenticate(String email, String password) throws InvoTellServiceException, InvoTellValidationException
	{
		try
		{
			if (email == null)
			{
				InvoTellError error = new InvoTellError(InvoTellError.ERROR_BAD_REQUEST, "Email is required");
				InvoTellValidationException validationException = new InvoTellValidationException(error.getMessage());
				validationException.getInvoTellErrors().add(error);
				throw validationException;
			}

			User existingUser = userRepository.findUserByEmail(email);

			if (!email.equals(existingUser.getEmail()))
			{
				InvoTellServiceException serviceException = new InvoTellServiceException(
						"Invalid username or password");
				throw serviceException;
			}

			if (existingUser.getPassword().equals(password))
			{
				InvoTellServiceException serviceException = new InvoTellServiceException(
						"Invalid username or password");
				throw serviceException;
			}
		} catch (InvoTellValidationException validationException)
		{
			throw validationException;
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellServiceException(e);
		}
		return null;
	}

	
	private void verifySkillExist(List<Skill> userSkill) throws InvoTellValidationException
	{
		try
		{
			if(userSkill!=null && userSkill.size()>0)
			{
				Iterator<Skill> skillIterator = userSkill.iterator();
				while (skillIterator.hasNext())
				{
					Skill skill = (Skill) skillIterator.next();
					skillService.getSkillByName(skill.getName(), skill.getSkillCategory());
				}
			}
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellValidationException(e);
		}
	}
	
	private User updateEntity(User updatingUser, User existingUser)
	{
		existingUser.setEmail(updatingUser.getEmail());
		existingUser.setExtension(updatingUser.getExtension());
		existingUser.setFirstName(updatingUser.getFirstName());
		existingUser.setLastName(updatingUser.getLastName());
		existingUser.setPhone(updatingUser.getPhone());
//		existingUser.setSipID(updatingUser.getSipID());
		return existingUser;
	}

}
