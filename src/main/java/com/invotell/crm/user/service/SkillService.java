package com.invotell.crm.user.service;

import java.util.List;

import com.invotell.crm.comm.exception.InvoTellServiceException;
import com.invotell.crm.comm.exception.InvoTellValidationException;
import com.invotell.crm.comm.exception.NotFoundException;
import com.invotell.crm.user.dao.entity.Skill;

public interface SkillService
{
	Skill addNewSkill(Skill skill,String categoryName) throws InvoTellServiceException, InvoTellValidationException;

	List<Skill> getAllSkill() throws InvoTellServiceException, NotFoundException;

	void deleteSkill(String skillName,String categoryName) throws InvoTellServiceException, InvoTellValidationException, NotFoundException;

	Skill getSkillByName(String skillName,String categoryName)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException;
	
	List<Skill> getAllSkillByCategory(String categoryName) throws InvoTellServiceException,InvoTellValidationException, NotFoundException;
}
