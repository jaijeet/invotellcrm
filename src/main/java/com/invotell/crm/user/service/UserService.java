package com.invotell.crm.user.service;

import java.util.List;

import com.invotell.crm.comm.exception.InvoTellServiceException;
import com.invotell.crm.comm.exception.InvoTellValidationException;
import com.invotell.crm.comm.exception.NotFoundException;
import com.invotell.crm.user.dao.entity.User;

public interface UserService
{
	User addNewUser(User user) throws InvoTellServiceException, InvoTellValidationException;

	User getUserByEmail(String email) throws InvoTellServiceException, InvoTellValidationException, NotFoundException;

	List<User> getAllUsers(boolean fetchSkills) throws InvoTellServiceException, NotFoundException;

	User updateUser(String email, User user)
			throws InvoTellServiceException, InvoTellValidationException, NotFoundException;

	void deleteUser(String email) throws InvoTellServiceException, InvoTellValidationException, NotFoundException;

	User authenticate(String email, String password) throws InvoTellServiceException, InvoTellValidationException;
}
