package com.invotell.crm.user.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.invotell.crm.constant.PersistanceConstant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = PersistanceConstant.AGENT_BREAK_TABLE)
public class AgentBreak
{
	private static final long serialVersionUID = 1L;
	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "break_name")
	private String breakName;
	
	@Column(name = "break_duration")
	private int breakDuration;
	
	@Column(name = "system_default")
	private boolean systemDefault;
	
	@Column(name = "hidden")
	private boolean hidden;
}
