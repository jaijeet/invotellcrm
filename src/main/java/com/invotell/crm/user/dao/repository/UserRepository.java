package com.invotell.crm.user.dao.repository;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.invotell.crm.user.dao.repository.jpa.UserJpaRepository;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.invotell.crm.comm.exception.InvoTellPersistanceException;
import com.invotell.crm.user.dao.entity.QUser;
import com.invotell.crm.user.dao.entity.Skill;
import com.invotell.crm.user.dao.entity.User;

@Repository
public class UserRepository
{
	private static final Logger LOGGER = LoggerFactory.getLogger(UserRepository.class);

	@Autowired
	private UserJpaRepository userJpaRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public User saveUser(User userToSave) throws InvoTellPersistanceException
	{
		LOGGER.info("Inside the UserRepository->saveUser(User userToSave)");
		User result = null;
		try
		{
			result = userJpaRepository.saveAndFlush(userToSave);
			entityManager.clear();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(readOnly = true)
	public User findUserByEmail(String email) throws InvoTellPersistanceException
	{
		LOGGER.info("Inside the UserRepository->sindUserByEmail(String email)");
		User result = null;
		try
		{
			BooleanExpression expression = QUser.user.email.eq(email);
			Optional<User> user = userJpaRepository.findOne(expression);
			if (user.isPresent())
			{
				result = user.get();
			}
			entityManager.clear();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(readOnly = true)
	public List<User> findAllUser(boolean fetchSkills) throws InvoTellPersistanceException
	{
		List<User> result = null;
		try
		{
			result = userJpaRepository.findAll();
			if(fetchSkills)
			{
				Iterator<User> userIterator = result.iterator();
				while (userIterator.hasNext())
				{
					User user = (User) userIterator.next();
					Iterator<Skill> skillIterator = user.getUserSkills().iterator();
					while (skillIterator.hasNext())
					{
						skillIterator.next();
					}		
				}
			}
			entityManager.clear();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public void deleteUser(User userToDelete) throws InvoTellPersistanceException
	{
		LOGGER.info("Inside the UserRepository->saveUser(User userToSave)");
		try
		{
			userJpaRepository.delete(userToDelete);
			entityManager.flush();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
	}
}
