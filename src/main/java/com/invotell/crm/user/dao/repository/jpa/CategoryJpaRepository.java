package com.invotell.crm.user.dao.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.invotell.crm.user.dao.entity.Category;

public interface CategoryJpaRepository extends JpaRepository<Category, Long>,QuerydslPredicateExecutor<Category>
{

}
