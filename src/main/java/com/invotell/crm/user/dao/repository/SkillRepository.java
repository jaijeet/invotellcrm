package com.invotell.crm.user.dao.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.invotell.crm.comm.exception.InvoTellPersistanceException;
import com.invotell.crm.user.dao.entity.QSkill;
import com.invotell.crm.user.dao.entity.Skill;
import com.invotell.crm.user.dao.repository.jpa.SkillJpaRepository;
import com.querydsl.core.types.dsl.BooleanExpression;

@Repository
public class SkillRepository
{
	private static final Logger LOGGER = LoggerFactory.getLogger(SkillRepository.class);
	
	@Autowired
	private SkillJpaRepository skillJpaRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public Skill addNewSkill(Skill skillToAdd) throws InvoTellPersistanceException
	{
		Skill result = null;
		try
		{
			result = skillJpaRepository.saveAndFlush(skillToAdd);
			entityManager.clear();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(readOnly = true)
	public Skill getSkillByName(String skillName,String categoryName) throws InvoTellPersistanceException
	{
		Skill result = null;
		try
		{
			BooleanExpression expression = QSkill.skill.name.eq(skillName).and(QSkill.skill.skillCategory.eq(categoryName));
			Optional<Skill> skill = skillJpaRepository.findOne(expression);
			if (skill.isPresent())
			{
				result = skill.get();
			}
			entityManager.clear();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(readOnly = true)
	public List<Skill> getAllSkillList() throws InvoTellPersistanceException
	{
		List<Skill> result = null;
		try
		{
			result = skillJpaRepository.findAll();
			entityManager.clear();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(readOnly = true)
	public List<Skill> getSkillListByCategoryName(String categoryName) throws InvoTellPersistanceException
	{
		List<Skill> result = null;
		try
		{
			BooleanExpression expression = QSkill.skill.skillCategory.eq(categoryName);
			result = (List<Skill>) skillJpaRepository.findAll(expression);
			entityManager.clear();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}
	
	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public void deleteSkill(Skill skillToDelete) throws InvoTellPersistanceException
	{
		try
		{
			skillJpaRepository.delete(skillToDelete);
			entityManager.flush();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
	}
}
