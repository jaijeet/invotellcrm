package com.invotell.crm.user.dao.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.invotell.crm.constant.PersistanceConstant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = PersistanceConstant.USER_TABLE)
public class User
{
	private static final long serialVersionUID = 1L;

	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "is_deleted", nullable = false)
	private boolean isDeleted;

	@Column(name = "logon_id", unique = true)
	private String logonID;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "phone")
	private String phone;

	@Column(name = "password")
	private String password;

	@Column(name = "email")
	private String email;

	@Column(name = "phone_format")
	private String phoneFormat;

	@Column(name = "extension")
	private String extension;

	@Column(name = "popup_flag")
	private boolean popupFlag;

	@Column(name = "first_time_login_flag")
	private boolean firstTimeLoginFlag;

	@Column(name = "time_zone_name")
	private String timeZoneName;

	@Column(name = "time_zone")
	private String timeZone;

	@ManyToMany(cascade = { CascadeType.ALL })
	@JoinTable(name = PersistanceConstant.USER_SKILLS_TABLE, joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "skill_id", referencedColumnName = "id"))
	private List<Skill> userSkills;

//    private Role role;
//    private Login logs; 
//    private SysCompany belongsTo;
//    private ClientLocation location;
//    private Set providerinfoDetails;
//    private Set timeSpent;
//    private Set assignments;

//	@Column(name="")
//	private boolean isSystemDefault;
//	
//	@Column(name="")
//	private String authToken;
//	
//	@Column(name="")
//	private String latestAuthToken;
//	
//	@Column(name="")
//	private String dateTimeDisplayFormat;

}
