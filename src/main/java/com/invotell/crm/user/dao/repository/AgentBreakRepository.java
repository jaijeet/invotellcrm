package com.invotell.crm.user.dao.repository;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.invotell.crm.comm.exception.InvoTellPersistanceException;
import com.invotell.crm.user.dao.entity.AgentBreak;
import com.invotell.crm.user.dao.entity.QAgentBreak;
import com.invotell.crm.user.dao.repository.jpa.AgentBreakJpaRepository;
import com.querydsl.core.types.dsl.BooleanExpression;

@Repository
public class AgentBreakRepository
{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AgentBreakRepository.class);
	
	@Autowired
	private AgentBreakJpaRepository agentBreakJpaRepository;

	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional(rollbackFor = InvoTellPersistanceException.class)
	public AgentBreak saveAgentBreak(AgentBreak agentBreak) throws InvoTellPersistanceException
	{
		AgentBreak result = null;
		try
		{
			result = agentBreakJpaRepository.saveAndFlush(agentBreak);
			entityManager.clear();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}
	
	@Transactional(readOnly = true, rollbackFor = InvoTellPersistanceException.class)
	public AgentBreak findBreakByName(String breakName) throws InvoTellPersistanceException
	{
		AgentBreak result = null;
		try
		{
			BooleanExpression expression = QAgentBreak.agentBreak.breakName.eq(breakName);
			Optional<AgentBreak> optional = agentBreakJpaRepository.findOne(expression);
			if (optional.isPresent())
			{
				result = optional.get();
			}
			entityManager.clear();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(rollbackFor = InvoTellPersistanceException.class)
	public void deleteAgentBreak(AgentBreak existingAgentBreak) throws InvoTellPersistanceException
	{	
		try
		{
			agentBreakJpaRepository.deleteById(existingAgentBreak.getId());
			entityManager.flush();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
	}
	
	
}
