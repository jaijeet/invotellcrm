package com.invotell.crm.user.dao.repository;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.invotell.crm.comm.exception.InvoTellPersistanceException;
import com.invotell.crm.user.dao.entity.Category;
import com.invotell.crm.user.dao.entity.QCategory;
import com.invotell.crm.user.dao.entity.Skill;
import com.invotell.crm.user.dao.repository.jpa.CategoryJpaRepository;
import com.querydsl.core.types.dsl.BooleanExpression;

@Repository
public class CategoryRepository
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryRepository.class);

	@Autowired
	private CategoryJpaRepository categoryJpaRepository;

	@PersistenceContext
	private EntityManager entityManager;

	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public Category saveCategory(Category categoryToSave) throws InvoTellPersistanceException
	{
		Category result = null;
		try
		{
			result = categoryJpaRepository.saveAndFlush(categoryToSave);
			entityManager.clear();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(readOnly = true)
	public Category findCategoryByName(String name,boolean fetchSkill) throws InvoTellPersistanceException
	{
		Category result = null;
		try
		{
			BooleanExpression expression = QCategory.category.name.eq(name);
			Optional<Category> category = categoryJpaRepository.findOne(expression);
			if (category.isPresent())
			{
				result = category.get();
			}
			
			if(fetchSkill)
			{
				Iterator<Skill> skillIterator = result.getSkills().iterator();
				while(skillIterator.hasNext())
				{
					skillIterator.next();
				}
			}
			entityManager.clear();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(readOnly = true)
	public List<Category> findAllCategories(boolean fetchSkill) throws InvoTellPersistanceException
	{
		List<Category> result = null;
		try
		{
			result = categoryJpaRepository.findAll();
			
			Iterator<Category> categoryIterator = result.iterator();
			while (categoryIterator.hasNext())
			{
				Category category = categoryIterator.next();
				if(fetchSkill)
				{
					Iterator<Skill> skillIterator = category.getSkills().iterator();
					
					while (skillIterator.hasNext())
					{
						skillIterator.next();
					}
				}
			}
			entityManager.clear();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
		return result;
	}

	@Transactional(noRollbackFor = InvoTellPersistanceException.class)
	public void deleteCategory(Category categoryToDelete) throws InvoTellPersistanceException
	{
		try
		{
			categoryJpaRepository.delete(categoryToDelete);
			entityManager.flush();
		} catch (Exception e)
		{
			LOGGER.error(e.getMessage(), e);
			throw new InvoTellPersistanceException(e.getMessage());
		}
	}
}
