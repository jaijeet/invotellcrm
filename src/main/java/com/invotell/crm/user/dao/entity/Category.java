package com.invotell.crm.user.dao.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.invotell.crm.constant.PersistanceConstant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = PersistanceConstant.CATEGORY_TABLE)
public class Category
{
	private static final long serialVersionUID = 1L;
	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name="category_name",unique = true)
    private String name;
	
	@OneToMany(fetch = FetchType.LAZY,orphanRemoval = true)
	@Cascade({CascadeType.DETACH})
	@JoinColumn(name = "category_name",insertable = false,updatable = false)
	private List<Skill> skills =new ArrayList<Skill>(0);
}
