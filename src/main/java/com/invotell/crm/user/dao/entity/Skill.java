package com.invotell.crm.user.dao.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.invotell.crm.constant.PersistanceConstant;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = PersistanceConstant.SKILL_TABLE)
public class Skill
{
	private static final long serialVersionUID = 1L;
	
	@JsonIgnore
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name="skill_name")
	private String name;
	
	@Column(name="skill_description")
	private String description;
	
	@Column(name="category_name")
	private String skillCategory;

	@JsonIgnore
	@ManyToMany(mappedBy = "userSkills")
	private List<User> users = new ArrayList<User>();

}
