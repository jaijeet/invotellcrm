package com.invotell.crm.user.dao.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.invotell.crm.user.dao.entity.AgentBreak;

public interface AgentBreakJpaRepository extends JpaRepository<AgentBreak, Long>, QuerydslPredicateExecutor<AgentBreak>
{

}
