package com.invotell.crm.user.dao.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.invotell.crm.user.dao.entity.User;



public interface UserJpaRepository extends JpaRepository<User, Long>, QuerydslPredicateExecutor<User>
{
	
}
