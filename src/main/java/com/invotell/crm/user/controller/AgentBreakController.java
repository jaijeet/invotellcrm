package com.invotell.crm.user.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.invotell.crm.comm.exception.InvoTellException;
import com.invotell.crm.comm.model.RequestInfo;
import com.invotell.crm.comm.model.ResponseInfo;
import com.invotell.crm.user.dao.entity.AgentBreak;
import com.invotell.crm.user.service.AgentBreakService;

@CrossOrigin
@RestController
@RequestMapping(value = "/v1/crm/agentbreak")
public class AgentBreakController
{
	
	@Autowired
	private AgentBreakService agentBreakService;
	
	
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseInfo<String>> addNewAgentBreak(@RequestBody RequestInfo<AgentBreak> requestInfo)
			throws InvoTellException
	{
		String response = null;
		try
		{
			AgentBreak entity = requestInfo.getRequestInfo();
			if (entity != null)
			{
				agentBreakService.addNewAgentBreak(entity);
				response = "Agent break is added";
			}
		} catch (Exception e)
		{
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<String>>(new ResponseInfo<String>(response), HttpStatus.CREATED);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseInfo<AgentBreak>> getAgentBreakByName(@RequestParam("breakName") String breakName)
			throws InvoTellException
	{
		AgentBreak response = null;
		try
		{
			response = agentBreakService.getAgentBreakByName(breakName); 
		} catch (Exception e)
		{
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<AgentBreak>>(new ResponseInfo<AgentBreak>(response), HttpStatus.CREATED);
	}
	
	
	@RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseInfo<String>> deleteAgentBreak(@RequestParam("breakName") String breakName)
			throws InvoTellException
	{
		String response = null;
		try
		{
			agentBreakService.deleteAgentBreak(breakName); 
			response = "Agent break is deleted";
		} catch (Exception e)
		{
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<String>>(new ResponseInfo<String>(response), HttpStatus.CREATED);
	}
}
