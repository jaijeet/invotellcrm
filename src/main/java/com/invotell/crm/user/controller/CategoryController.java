package com.invotell.crm.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.invotell.crm.comm.exception.InvoTellException;
import com.invotell.crm.comm.model.RequestInfo;
import com.invotell.crm.comm.model.ResponseInfo;
import com.invotell.crm.user.dao.entity.Category;
import com.invotell.crm.user.dao.entity.Skill;
import com.invotell.crm.user.service.CategoryService;
import com.invotell.crm.user.service.SkillService;

@CrossOrigin
@RestController
@RequestMapping(value = "/v1/crm/category")
public class CategoryController
{
	@Autowired
	private CategoryService categoryService;

	@Autowired
	private SkillService skillService;

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseInfo<String>> addNewCategory(@RequestBody RequestInfo<Category> requestInfo)
			throws InvoTellException
	{
		String response = null;
		try
		{
			Category entity = requestInfo.getRequestInfo();
			if (entity != null)
			{
				categoryService.addNewCategory(entity);
				response = "Category is added";
			}
		} catch (Exception e)
		{
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<String>>(new ResponseInfo<String>(response), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseInfo<Category>> getCategoryByName(@RequestParam("name") String name)
			throws InvoTellException
	{
		Category response = null;
		try
		{
			response = categoryService.getCategoryByName(name);

		} catch (Exception e)
		{
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<Category>>(new ResponseInfo<Category>(response), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.DELETE, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseInfo<String>> deleteUserByName(@RequestParam("name") String name)
			throws InvoTellException
	{
		String result = null;
		try
		{
			categoryService.deleteCategory(name);
			result = "Category is deleted";
		} catch (Exception e)
		{
			throw new InvoTellException(e.getMessage());
		}

		return new ResponseEntity<ResponseInfo<String>>(new ResponseInfo<String>(result), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "all")
	public ResponseEntity<ResponseInfo<List<Category>>> getAllCategories() throws InvoTellException
	{
		List<Category> result = null;
		try
		{
			result = categoryService.getAllCategory(true);
		} catch (Exception e)
		{
			throw new InvoTellException(e.getMessage());
		}

		return new ResponseEntity<ResponseInfo<List<Category>>>(new ResponseInfo<List<Category>>(result),
				HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, path = "skill")
	public ResponseEntity<ResponseInfo<String>> addNewSkill(@RequestBody RequestInfo<Skill> requestInfo,
			@RequestParam("categoryName") String categoryName) throws InvoTellException
	{
		String response = null;
		try
		{
			Skill entity = requestInfo.getRequestInfo();
			if (entity != null)
			{
				skillService.addNewSkill(entity, categoryName);
				response = "Skill is added";
			}
		} catch (Exception e)
		{
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<String>>(new ResponseInfo<String>(response), HttpStatus.CREATED);
	}

	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "skill")
	public ResponseEntity<ResponseInfo<Skill>> getSkillByName(@RequestParam("skillName") String skillName,
			@RequestParam("categoryName") String categoryName) throws InvoTellException
	{
		Skill response = null;
		try
		{
			response = skillService.getSkillByName(skillName, categoryName);

		} catch (Exception e)
		{
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<Skill>>(new ResponseInfo<Skill>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE, path = "skill")
	public ResponseEntity<ResponseInfo<String>> deleteSkillByName(@RequestParam("skillName") String skillName,
			@RequestParam("categoryName") String categoryName) throws InvoTellException
	{
		String response = null;
		try
		{
			skillService.deleteSkill(skillName, categoryName);
			response = "Skill named ["+ skillName +"] is deleted";

		} catch (Exception e)
		{
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<String>>(new ResponseInfo<String>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "skill/all")
	public ResponseEntity<ResponseInfo<List<Skill>>> getAllSkills() throws InvoTellException
	{
		List<Skill> response = null;
		try
		{
			response = skillService.getAllSkill();

		} catch (Exception e)
		{
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<List<Skill>>>(new ResponseInfo<List<Skill>>(response), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, path = "skill/category")
	public ResponseEntity<ResponseInfo<List<Skill>>> getAllSkillsByCategory(@RequestParam("categoryName") String categoryName) throws InvoTellException
	{
		List<Skill> response = null;
		try
		{
			response = skillService.getAllSkillByCategory(categoryName);

		} catch (Exception e)
		{
			throw new InvoTellException(e.getMessage());
		}
		return new ResponseEntity<ResponseInfo<List<Skill>>>(new ResponseInfo<List<Skill>>(response), HttpStatus.OK);
	}
	
}
