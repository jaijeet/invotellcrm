package com.invotell.crm.handler;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.invotell.crm.comm.exception.InvoTellException;
import com.invotell.crm.comm.exception.InvoTellValidationException;
import com.invotell.crm.comm.exception.NotFoundException;
import com.invotell.crm.comm.model.InvoTellError;
import com.invotell.crm.comm.model.ResponseInfo;

public class RestControllerExceptionHandler extends ResponseEntityExceptionHandler
{
	private static final Logger LOGGER = LoggerFactory.getLogger(RestControllerAdvice.class);

	@ExceptionHandler(InvoTellException.class)
	public ResponseInfo<List<InvoTellError>> handleInvoTellException(InvoTellException ite, HttpServletRequest request,
			HttpServletResponse response)
	{
		ResponseInfo<List<InvoTellError>> responseInfo = new ResponseInfo<List<InvoTellError>>();
		List<InvoTellError> errors = new ArrayList<InvoTellError>(0);

		if (ite instanceof InvoTellValidationException)
		{
			InvoTellValidationException itve = (InvoTellValidationException) ite;
			errors.addAll(itve.getInvoTellErrors());
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Credentials", "true");
			response.setHeader("Access-Control-Allow-Methods", "GET, HEAD, OPTIONS, PUT, POST");
			response.setHeader("Access-Control-Allow-Headers",
					"Origin, Accept, X-Request-With, Content-Type, Access-Control-Request-Headers");
			response.setStatus(HttpStatus.BAD_REQUEST.value());

		} else if (ite instanceof NotFoundException)
		{
			errors.add(new InvoTellError(HttpStatus.NOT_FOUND.value(), ite.getMessage()));
			response.setStatus(HttpStatus.NOT_FOUND.value());
		} else
		{
			errors.add(new InvoTellError(HttpStatus.NOT_FOUND.value(), ite.getMessage()));
			response.setStatus(HttpStatus.NOT_FOUND.value());
			LOGGER.error(ite.getMessage(), ite);
		}

		responseInfo.setResponseInfo(errors);
		return responseInfo;
	}

	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request)
	{
		LOGGER.error(ex.getMessage(), ex);
		List<InvoTellError> errors = new ArrayList<InvoTellError>(0);
		errors.add(new InvoTellError(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage()));
		ResponseInfo<List<InvoTellError>> responseInfo = new ResponseInfo<List<InvoTellError>>(errors);
		return new ResponseEntity<Object>(responseInfo, HttpStatus.BAD_REQUEST);
	}

}
