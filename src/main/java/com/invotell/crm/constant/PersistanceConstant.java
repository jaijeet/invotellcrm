package com.invotell.crm.constant;

public interface PersistanceConstant
{
	public static String USER_TABLE = "user";
	public static String CATEGORY_TABLE = "category";
	public static String SKILL_TABLE="skill";
	public static String USER_SKILLS_TABLE="user_skill";
	public static String AGENT_BREAK_TABLE="agent_break";
}
