CREATE TABLE `invotellcrm`.`agent_break` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `break_name` VARCHAR(50) NULL,
  `break_duration` INT NOT NULL,
  `system_default` TINYINT NULL DEFAULT 1,
  `hidden` TINYINT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `break_name_UNIQUE` (`break_name` ASC));
