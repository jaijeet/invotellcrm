CREATE TABLE `invotellcrm`.`skill` (
  `id` BIGINT NOT NULL AUTO_INCREMENT,
  `skill_name` VARCHAR(50) NULL,
  `skill_description` VARCHAR(200) NULL,
  `category_name` VARCHAR(100) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_skill_category_idx_idx` (`category_name` ASC),
  CONSTRAINT `fk_skill_category_idx`
    FOREIGN KEY (`category_name`)
    REFERENCES `invotellcrm`.`category` (`category_name`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
